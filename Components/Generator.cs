﻿using LIF.PocoGenerator.Models.Generator;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.Pluralization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIF.PocoGenerator
{
    public class Generator
    {
        public DatabaseSchema Schema { get; protected set; }
        public List<PocoClass> Pocos { get; protected set; } = new List<PocoClass>();
        public EnglishPluralizationService PluralizationService { get; set; } = new EnglishPluralizationService();

        public string ContextClassName { get; set; }
        public string ContextBaseClassName { get; set; } = "System.Data.Entity.DbContext";
        public string ConnectionStringName { get; set; }
        public string FluentConfigurationSuffix { get; set; } = "FluentConfiguration";

        public Generator(DatabaseSchema schema)
        {
            Schema = schema;

            Init();
        }

        public void Init()
        {
            Pocos.Clear();

            // we don't generate poco's for many to many tables.
            // untill maybe EF decides we can map a many to many and a middle table as well :>
            // which I doubt alot since they are already dev EF 7.0 which is a whole new branch.
            var notManyToManyTables = Schema.Tables.Where(t => !t.IsManyToManyTable());

            foreach (var t in notManyToManyTables)
                Pocos.Add(new PocoClass(t, this));
        }

        public string GetPocoContextSetName(PocoClass poco)
        {
            var plural = this.Pluralize(poco.ClassName);
            
            if (Pocos.Count(t => t.ClassName == poco.ClassName) > 1)
                return $"{poco.Namespace}{plural}";

            return plural;
        }

        public string Pluralize(string tableName)
        {
            var result = PluralizationService.Pluralize(tableName);
            return result;
        }
    }
}

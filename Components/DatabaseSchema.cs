﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using LIF.PocoGenerator.Models.Schema;

namespace LIF.PocoGenerator
{
    public class DatabaseSchema
    {
        private SqlConnection _connection = null;
        public List<TableInformation> Tables { get; protected set; } = new List<TableInformation>();
        public string ConnectionString { get; protected set; }


        public DatabaseSchema(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public void FetchSchema()
        {
            // clear tables.
            Tables.Clear();

            using (_connection = new SqlConnection(ConnectionString))
            {
                // open the connection
                _connection.Open();

                GetTables();
                GetColumns();
                GetForeignKeys();
                GetPrimaryKeys();
            }
        }

        protected void GetPrimaryKeys()
        {
            var primaryKeySQL = Scripts.FetchPrimaryKeys;

            // command.
            SqlCommand command = new SqlCommand(primaryKeySQL, _connection);
            SqlDataReader reader = command.ExecuteReader();

            // read table names.
            while (reader.Read())
            {
                // get the keys.
                var tableSchema = reader["TABLE_SCHEMA"] as string;
                var tableName = reader["TABLE_NAME"] as string;
                var columnName = reader["COLUMN_NAME"] as string;

                // find the table.
                var table = Tables.Where(t => t.Schema == tableSchema && t.Name == tableName).FirstOrDefault();
                if (table == null)
                    continue;

                // find the column
                var column = table.Columns.Where(t => t.Name == columnName).FirstOrDefault();
                if (column == null)
                    throw new Exception($"could not find {columnName} inside table {tableSchema}.{tableName}");

                // add primary key.
                table.PrimaryKeys.Add(column);
            }

            // Call Close when done reading.
            reader.Close();
        }

     

        protected void GetTables()
        {
            var tablesSQL = Scripts.FetchTables;

            // command.
            SqlCommand command = new SqlCommand(tablesSQL, _connection);
            SqlDataReader reader = command.ExecuteReader();

            // read table names.
            while (reader.Read())
            {
                // create a new table.
                var t = new TableInformation
                {
                    Schema = reader["TABLE_SCHEMA"] as string,
                    Name = reader["TABLE_NAME"] as string
                };

                // skip diagram table.
                if (t.Schema == "dbo" && t.Name == "sysdiagrams")
                    continue;

                // add the table.
                    Tables.Add(t);
            }

            // Call Close when done reading.
            reader.Close();
        }

        protected void GetColumns()
        {
            var columnsSQL = Scripts.FetchColumns;

            // command.
            SqlCommand command = new SqlCommand(columnsSQL, _connection);
            SqlDataReader reader = command.ExecuteReader();

            // table and schema name.
            string tableName;
            string schemaName;
            TableInformation table = null;

            // read table names.
            while (reader.Read())
            {
                // set schema and table name.
                schemaName = reader["TABLE_SCHEMA"] as string;
                tableName = reader["TABLE_NAME"] as string;

                // find table.
                table = Tables.Where(t => t.Schema == schemaName && t.Name == tableName).FirstOrDefault();
                if (table == null)
                    continue;

                // set dynamic stuff.
                var column = new ColumnInformation();
                column.Table = table;
                column.Nullable = (reader["IS_NULLABLE"] as string) == "YES";
                column.DataType = reader["DATA_TYPE"] as string;
                column.Name = reader["COLUMN_NAME"] as string;
                column.IsIdentity = reader["IS_IDENTITY"] is DBNull ? false : Convert.ToBoolean(reader["IS_IDENTITY"]);

                // default.
                if (false == (reader["COLUMN_DEFAULT"] is DBNull))
                    column.DefaultValue = reader["COLUMN_DEFAULT"] as string;

                // max char size.
                if (false == (reader["CHARACTER_MAXIMUM_LENGTH"] is DBNull))
                    column.CharacterMaximumLength = reader["CHARACTER_MAXIMUM_LENGTH"] as int?;

                // numeric precision stuff.
                if (false == (reader["NUMERIC_PRECISION"] is DBNull))
                    column.NumericPrecision = Convert.ToByte(reader["NUMERIC_PRECISION"]);

                if (false == (reader["NUMERIC_SCALE"] is DBNull))
                    column.NumericScale = Convert.ToByte(reader["NUMERIC_SCALE"]);

                // add the column
                table.Columns.Add(column);
            }

            // Call Close when done reading.
            reader.Close();
        }

        protected void GetForeignKeys()
        {
            var foreignKeysSQL = Scripts.FetchForeignKeys;

            // command.
            SqlCommand command = new SqlCommand(foreignKeysSQL, _connection);
            SqlDataReader reader = command.ExecuteReader();

            // read table names.
            while (reader.Read())
            {
                // fk 
                var fkSchemaName = reader["FKSchema"] as string;
                var fkTableName = reader["FKTable"] as string;
                var fkColumnName = reader["FKColumn"] as string;

                // pk
                var pkSchemaName = reader["PKSchema"] as string;
                var pkTableName = reader["PKTable"] as string;
                var pkColumnName = reader["PKColumn"] as string;

                // fk table
                var fkTableInfo = Tables.Where(t => t.Schema == fkSchemaName && t.Name == fkTableName).FirstOrDefault();
                var pkTableInfo = Tables.Where(t => t.Schema == pkSchemaName && t.Name == pkTableName).FirstOrDefault();

                // if not found just skip (prob means it was filtered somewhere in the drain)
                if (pkTableInfo == null || fkTableInfo == null)
                    continue;

                // find the fk column.
                var fkColumn = fkTableInfo.Columns.Where(t => t.Name == fkColumnName).FirstOrDefault();
                if (fkColumn == null)
                    throw new Exception($"Could not find {fkColumnName} in table {fkSchemaName}.{fkTableName}");

                // find the pk column 
                var pkColumn = pkTableInfo.Columns.Where(t => t.Name == pkColumnName).FirstOrDefault();
                if (pkColumn == null)
                    throw new Exception($"Could not find {pkColumnName} in table {pkSchemaName}.{pkColumnName}");

                // create foreign key information
                var foreignKey = new ForeignKeyInformation
                {
                    PrimaryKeyColumn = pkColumn,
                    ForeignKeyColumn = fkColumn
                };

                // add to foreign key inside the pk.
                fkTableInfo.ForeignKeys.Add(foreignKey);
                pkTableInfo.ReverseNavigation.Add(foreignKey);
            }

            // Call Close when done reading.
            reader.Close();
        }
    }
}

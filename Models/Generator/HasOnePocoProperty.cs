﻿using LIF.PocoGenerator.Models.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIF.PocoGenerator.Models.Generator
{
    public class HasOnePocoProperty : INavigationPocoProperty
    {
        public ForeignKeyInformation ForeignKey { get; protected set; }
        public PocoClass Poco { get; protected set; }

        public HasOnePocoProperty(ForeignKeyInformation foreignKey, PocoClass poco)
        {
            this.ForeignKey = foreignKey;
            this.Poco = poco;

            // set property name.
            this.PropertyName = this.GetPropertyName();
        }

        public PocoPropertyTypes PocoPropertyType
        {
            get
            {
                return PocoPropertyTypes.HasOne;
            }
        }

        public string PropertyName
        {
            get; protected set;
        }

        public string PropertyType
        {
            get
            {
                return $"{ForeignKey.ForeignKeyColumn.Table.Schema}.{ForeignKey.ForeignKeyColumn.Table.Name}";
            }
        }

        public string GetGeneratedPropertyLine()
        {
            return $"public virtual {PropertyType} {PropertyName} {{ get; set; }}";
        }

        protected string GetPropertyName()
        {
            string ret = ForeignKey.ForeignKeyColumn.Table.Name;
            string possibility;

            if (this.IsPropertyTaken(ret))
            {
                for(var i = 1; i <= int.MaxValue; i++)
                {
                    possibility = $"{ret}_{i}";
                    if (!this.IsPropertyTaken(possibility))
                    {
                        ret = possibility;
                        break;
                    }
                }
            }

            return ret;
        }
    }
}

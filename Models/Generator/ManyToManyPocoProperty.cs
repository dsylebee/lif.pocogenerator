﻿using System;
using LIF.PocoGenerator.Models.Schema;
using System.Linq;

namespace LIF.PocoGenerator.Models.Generator
{
    public class ManyToManyPocoProperty : INavigationPocoProperty
    {
        public ForeignKeyInformation ForeignKey { get; protected set; }
        public PocoClass Poco { get; protected set; }

        public ManyToManyPocoProperty(ForeignKeyInformation foreignKey, PocoClass poco)
        {
            this.ForeignKey = foreignKey;
            this.Poco = poco;

            // set property name.
            this.PropertyName = this.GetPropertyName();
            this.PropertyType = this.GetPropertyType();
        }

        public PocoPropertyTypes PocoPropertyType
        {
            get
            {
                return PocoPropertyTypes.ManyToMany;
            }
        }

        public string PropertyName
        {
            get; protected set;
        }

        public string PropertyType
        {
            get; protected set;
        }

        public string GetGeneratedPropertyLine()
        {
            return $"public virtual System.Collections.Generic.ICollection<{PropertyType}> {PropertyName} {{ get; set; }} = new System.Collections.Generic.List<{PropertyType}>();";
        }

        protected string GetPropertyName()
        {
            // the foreign table.
            var foreignTable = ForeignKey.ForeignKeyColumn.Table;

            // find the other foreign key
            var other = foreignTable.ForeignKeys
                .Where(t => t != ForeignKey)
                .First();

            // reverse table name.
            var tableName = other.PrimaryKeyColumn.Table.Name;

            // plurialize.
            string ret = Poco.Generator.Pluralize(tableName);
            string possibility;

            if (this.IsPropertyTaken(ret))
            {
                for (var i = 1; i <= int.MaxValue; i++)
                {
                    possibility = $"{ret}_{i}";
                    if (!this.IsPropertyTaken(possibility))
                    {
                        ret = possibility;
                        break;
                    }
                }
            }

            return ret;
        }

        protected string GetPropertyType()
        {
            // the foreign table.
            var foreignTable = ForeignKey.ForeignKeyColumn.Table;

            // find the other foreign key
            var other = foreignTable.ForeignKeys
                .Where(t => t != ForeignKey)
                .First();
            
            // thats the reverse navigation :)
            var ret = $"{other.PrimaryKeyColumn.Table.Schema}.{other.PrimaryKeyColumn.Table.Name}";

            return ret;
        }
    }
}
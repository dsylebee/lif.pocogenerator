﻿using LIF.PocoGenerator.Models.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIF.PocoGenerator.Models.Generator
{
    public enum PocoPropertyTypes
    {
        Column,
        ForeignKey,
        HasOne,
        HasMany,
        ManyToMany
    }

    public interface IPocoProperty
    {
        PocoPropertyTypes PocoPropertyType { get; }
        string PropertyName { get; }
        string PropertyType { get; }
        string GetGeneratedPropertyLine();
    }

    public interface INavigationPocoProperty : IPocoProperty
    {
        PocoClass Poco { get; }
    }

    public static class PocoPropertyExtensions
    {
        public static bool IsPropertyTaken(this INavigationPocoProperty that, string propertyName)
        {
            return that.Poco.Properties.Any(t => t != that && t.PropertyName == propertyName);
        }

        public static string GetCommentType(this IPocoProperty that)
        {
            if (that.PocoPropertyType == PocoPropertyTypes.Column)
                return "Column";
            else if (that.PocoPropertyType == PocoPropertyTypes.ForeignKey)
                return "Foreign Key";
            else if (that.PocoPropertyType == PocoPropertyTypes.HasOne)
                return "Has one (one to one)";
            else if (that.PocoPropertyType == PocoPropertyTypes.HasMany)
                return "Has Many";
            else if (that.PocoPropertyType == PocoPropertyTypes.ManyToMany)
                return "Many to Many";

            return "";
        }
    }
}

﻿using LIF.PocoGenerator.Models.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIF.PocoGenerator.Models.Generator
{
    public class PocoClass
    {
        public TableInformation Table { get; protected set; }
        public List<IPocoProperty> Properties { get; protected set; } = new List<IPocoProperty>();
        public PocoGenerator.Generator Generator { get; protected set; }

        public PocoClass(TableInformation table, PocoGenerator.Generator generator)
        {
            // stuff
            Generator = generator;
            Table = table;

            // generate.
            Generate();
        }

        public bool HasProperties(PocoPropertyTypes type)
        {
            return Properties.Any(t => t.PocoPropertyType == type);
        }

        public IEnumerable<IPocoProperty> GetProperties(PocoPropertyTypes type)
        {
            return Properties.Where(t => t.PocoPropertyType == type);
        }

        public IEnumerable<ColumnPocoProperty> GetColumnProperties() => GetProperties(PocoPropertyTypes.Column).Cast<ColumnPocoProperty>();
        public IEnumerable<ForeignKeyPocoProperty> GetForeignKeyProperties() => GetProperties(PocoPropertyTypes.ForeignKey).Cast<ForeignKeyPocoProperty>();
        public IEnumerable<HasManyPocoProperty> GetHasManyProperties() => GetProperties(PocoPropertyTypes.HasMany).Cast<HasManyPocoProperty>();
        public IEnumerable<HasOnePocoProperty> GetHasOneProperties() => GetProperties(PocoPropertyTypes.HasOne).Cast<HasOnePocoProperty>();            
        public IEnumerable<ManyToManyPocoProperty> GetManyToManyProperties() => GetProperties(PocoPropertyTypes.ManyToMany).Cast<ManyToManyPocoProperty>();            

        protected virtual void Generate()
        {
            // columns
            foreach (var c in Table.Columns)
                Properties.Add(new ColumnPocoProperty(c));

            // foreign keys
            foreach (var fk in Table.ForeignKeys)
                Properties.Add(new ForeignKeyPocoProperty(fk, this));

            // has one
            foreach (var ho in Table.HasOne())
                Properties.Add(new HasOnePocoProperty(ho, this));

            // has many
            foreach (var hm in Table.HasMany())
                Properties.Add(new HasManyPocoProperty(hm, this));

            // many to many
            foreach (var m2m in Table.ManyToMany())
                Properties.Add(new ManyToManyPocoProperty(m2m, this));
        }
        
        // class name.
        public string ClassName
        {
            get
            {
                return Table.Name;
            }
        }

        public string Namespace
        {
            get
            {
                var ret = Table.Schema;
                return ret;
            }
        }

        public string ToTableFluent()
        {
            return $"ToTable(\"{Table.Schema}.{Table.Name}\");";
        }

        public string HasKeyFluent()
        {
            if (!Table.PrimaryKeys.Any())
            {
                return "";
            }

            var ret = "HasKey(x => ";

            if (Table.PrimaryKeys.Count > 1)
            {
                // start new anonymous object
                ret += "new {";

                foreach (var pk in Table.PrimaryKeys)
                    ret += $"x.{pk.Name}, ";

                // trim the last ,
                ret= ret.TrimEnd(',', ' ');

                // append end anonymous object.
                ret += "}";
            } 
            else
            {
                ret += $"x.{Table.PrimaryKeys.First().Name}";
            }            

            ret += ");";

            return ret;
        }

        public string ColumnPropertyFluent(ColumnPocoProperty p)
        {
            var ret = "";
      
            // property name.
            ret += $"Property(x => x.{p.PropertyName})";

            // column name.
            ret += $".HasColumnName(\"{p.ColumnInfo.Name}\")";

            // IS NULL
            if (p.ColumnInfo.Nullable)
                ret += ".IsOptional()";
            else
                ret += ".IsRequired()";

            // fixed length.
            if (p.ColumnInfo.IsFixedLength)
                ret += ".IsFixedLength()";

            if (p.ColumnInfo.IsStringType && !p.ColumnInfo.IsUnicode)
                ret += ".IsUnicode(false)";

            // sql data type.
            ret += $".HasColumnType(\"{p.ColumnInfo.DataType}\")";

            // max length.
            if (p.ColumnInfo.CharacterMaximumLength.HasValue && p.ColumnInfo.CharacterMaximumLength.Value > 0)
                ret += $".HasMaxLength({p.ColumnInfo.CharacterMaximumLength.Value})";

            // precision.
            if (p.ColumnInfo.NumericPrecision.HasValue && p.ColumnInfo.NumericScale.HasValue && !p.ColumnInfo.IsPrimitive)
                ret += $".HasPrecision({p.ColumnInfo.NumericPrecision}, {p.ColumnInfo.NumericScale})";

            // identity.
            if (p.ColumnInfo.IsIdentity)
                ret += ".HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)";
            else if (p.ColumnInfo.HasDefault && p.ColumnInfo.DefaultValue.Equals("(newsequentialid())", StringComparison.InvariantCultureIgnoreCase))
                ret += ".HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)";
            else if (p.ColumnInfo.HasDefault && p.ColumnInfo.DefaultValue.Equals("(newid())", StringComparison.InvariantCultureIgnoreCase))
                ret += ".HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)";
            else if (p.ColumnInfo.IsPrimaryKey)
                ret += ".HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)";

            ret += ";";

            return ret;
        }

        private string GetHasOneForeignKeyFluent(ForeignKeyPocoProperty fk)
        {
            // reverse navigation.
            var poco = Generator.Pocos
                .Where(t => t.Table == fk.ForeignKey.PrimaryKeyColumn.Table)
                .First();

            // has one.
            var hasOne = poco
                .GetHasOneProperties()
                .Where(t => t.ForeignKey == fk.ForeignKey)
                .First();

            // return value.
            var ret = "";

            // fluent beg.
            ret += $"HasRequired(a => a.{fk.PropertyName})";

            // reverse navigation
            ret += $".WithOptional(b => b.{hasOne.PropertyName})";

            // ;
            ret += ";";
 
            return ret;
        }

        private string GetHasManyForeignKeyFluent(ForeignKeyPocoProperty fk)
        {
            // reverse navigation.
            var poco = Generator.Pocos
                .Where(t => t.Table == fk.ForeignKey.PrimaryKeyColumn.Table)
                .First();

            // find the has man relation.
            var hasManyRelationProperty = poco
                .GetHasManyProperties()
                .Where(t => t.ForeignKey == fk.ForeignKey)
                .First();

            // return value.
            var ret = "";

            // fluent beg.
            if (fk.ForeignKey.ForeignKeyColumn.Nullable)
                ret += $"HasOptional(a => a.{fk.PropertyName})";
            else
                ret += $"HasRequired(a => a.{fk.PropertyName})";

            // reverse navigation
            ret += $".WithMany(b => b.{hasManyRelationProperty.PropertyName})";

            // which property is the foreign key.
            ret += $".HasForeignKey(c => c.{fk.ForeignKey.ForeignKeyColumn.Name})";

            // ;
            ret += ";";

            return ret;
        }

        private string GetManyToManyForeignKeyFluent(ForeignKeyPocoProperty fk)
        {
            // return value.
            var ret = "";

            ret += $"HasRequired(a => a.{fk.PropertyName})";

            // which property is the foreign key.
            ret += $".WithMany().HasForeignKey(c => c.{fk.ForeignKey.ForeignKeyColumn.Name})";

            // ;
            ret += ";";

            return ret;
        }

        public List<string> GetForeignKeysFluent()
        {
            var ret = new List<string>();

            // foreign keys.
            var fks = this.GetForeignKeyProperties();
            foreach(var fk in fks)
            {
                if (fk.ForeignKey.IsHasOne())
                    ret.Add(GetHasOneForeignKeyFluent(fk));
                else if (fk.ForeignKey.IsHasMany())
                    ret.Add(GetHasManyForeignKeyFluent(fk));

                /*else if (fk.ForeignKey.IsManyToMany())
                    ret.Add(GetManyToManyForeignKeyFluent(fk));*/
            }

            return ret;
        }

        protected string GetManyToManyFluent(ManyToManyPocoProperty mtm)
        {
            var ret = "";

            // the many to many table.
            var table = mtm.ForeignKey.ForeignKeyColumn.Table;

            // find the other foreign key inside that table.
            var otherFK = table.ForeignKeys.Where(t => t != mtm.ForeignKey).First();

            // find the poco for that table.
            var otherPoco = this.Generator.Pocos.Where(t => t.Table == otherFK.PrimaryKeyColumn.Table).First();

            // find the other many to many property.
            var otherManyToManyProperty = otherPoco.GetManyToManyProperties().Where(t => t.ForeignKey == otherFK).First();

            // has many rule.
            ret += $"HasMany(t => t.{mtm.PropertyName})";
            ret += $".WithMany(t => t.{otherManyToManyProperty.PropertyName})";
            ret += ".Map(m => m";
            ret += $".ToTable(\"{table.Name}\", \"{table.Schema}\")";
            ret += $".MapLeftKey(\"{mtm.ForeignKey.ForeignKeyColumn.Name}\")";
            ret += $".MapRightKey(\"{otherFK.ForeignKeyColumn.Name}\")";
            ret += ");";

            return ret;
        }

        public List<string> GetManyToManyFluents()
        {
            var ret = new List<string>();

            // get the many to many.
            var mtms = this.GetManyToManyProperties();

            // final mtms (Only do the mapping for 1 out 2 relationship in the many to many table)
            var finalMtms = mtms.Where(t => t.ForeignKey.ForeignKeyColumn.PrimaryKeyOrder == 1);

            // get the many to many.
            foreach (var mtm in finalMtms)
                ret.Add(GetManyToManyFluent(mtm));

            return ret;
        }
    }
}

﻿using LIF.PocoGenerator.Models.Schema;

namespace LIF.PocoGenerator.Models.Generator
{
    public class HasManyPocoProperty : INavigationPocoProperty
    {
        public ForeignKeyInformation ForeignKey { get; protected set; }
        public PocoClass Poco { get; protected set; }

        public HasManyPocoProperty(ForeignKeyInformation foreignKey, PocoClass poco)
        {
            this.ForeignKey = foreignKey;
            this.Poco = poco;

            // set property name.
            this.PropertyName = this.GetPropertyName();
        }

        public PocoPropertyTypes PocoPropertyType
        {
            get
            {
                return PocoPropertyTypes.HasMany;
            }
        }

        public string PropertyName
        {
            get; protected set;
        }

        public string PropertyType
        {
            get
            {
                return $"{ForeignKey.ForeignKeyColumn.Table.Schema}.{ForeignKey.ForeignKeyColumn.Table.Name}";
            }
        }

        public string GetGeneratedPropertyLine()
        {
            return $"public virtual System.Collections.Generic.ICollection<{PropertyType}> {PropertyName} {{ get; set; }} = new System.Collections.Generic.List<{PropertyType}>();";
        }

        protected string GetPropertyName()
        {
            var tableName = ForeignKey.ForeignKeyColumn.Table.Name;
            string ret = Poco.Generator.Pluralize(tableName);
            string possibility;

            if (this.IsPropertyTaken(ret))
            {
                for (var i = 1; i <= int.MaxValue; i++)
                {
                    possibility = $"{ret}_{i}";
                    if (!this.IsPropertyTaken(possibility))
                    {
                        ret = possibility;
                        break;
                    }
                }
            }

            return ret;
        }
    }
}
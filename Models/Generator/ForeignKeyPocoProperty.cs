﻿using LIF.PocoGenerator.Models.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIF.PocoGenerator.Models.Generator
{
    public class ForeignKeyPocoProperty : INavigationPocoProperty
    {
        public ForeignKeyInformation ForeignKey { get; protected set; }
        public PocoClass Poco { get; protected set; }

        public ForeignKeyPocoProperty(ForeignKeyInformation foreignKey, PocoClass poco)
        {
            this.ForeignKey = foreignKey;
            this.Poco = poco;

            // set property name.
            this.PropertyName = this.GetPropertyName();
        }

        public PocoPropertyTypes PocoPropertyType
        {
            get
            {
                return PocoPropertyTypes.ForeignKey;
            }
        }

        public string PropertyName
        {
            get; protected set;
        }

        public string PropertyType
        {
            get
            {
                return $"{ForeignKey.PrimaryKeyColumn.Table.Schema}.{ForeignKey.PrimaryKeyColumn.Table.Name}";
            }
        }

        public string GetGeneratedPropertyLine()
        {
            return $"public virtual {PropertyType} {PropertyName} {{ get; set; }}";
        }

        protected string GetPropertyName()
        {
            var fkColumn = ForeignKey.ForeignKeyColumn;

            string possibleName;
            string ret;

            if (fkColumn.IsPrimaryKey)
                ret = ForeignKey.PrimaryKeyColumn.Table.Name;
            else
                ret = Helpers.Strings.RemoveFromEnd(fkColumn.Name, "Id");

            // there is a column name with the same name as attempted relationship.
            if (this.IsPropertyTaken(ret))
            {
                // check if possible (Preferred)
                possibleName = $"{ret}_{fkColumn.Name}";
                ret = possibleName;

                if (this.IsPropertyTaken(ret))
                {
                    for (var i = 1; i <= int.MaxValue; i++)
                    {
                        possibleName = $"{ret}{i}";
                        if (!this.IsPropertyTaken(possibleName))
                        {
                            ret = possibleName;
                            break;
                        }
                    }
                }
            }

            return ret;
        }
    }
}

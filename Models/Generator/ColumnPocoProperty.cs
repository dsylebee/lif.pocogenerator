﻿using LIF.PocoGenerator.Models.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIF.PocoGenerator.Models.Generator
{
    public class ColumnPocoProperty : IPocoProperty
    {
        public ColumnInformation ColumnInfo { get; protected set; }

        public ColumnPocoProperty(ColumnInformation columnInfo)
        {
            ColumnInfo = columnInfo;
        }

        public PocoPropertyTypes PocoPropertyType
        {
            get
            {
                return PocoPropertyTypes.Column;
            }
        }

        public string PropertyName
        {
            get
            {
                return ColumnInfo.Name;
            }
        }

        public string PropertyType
        {
            get
            {
                return ColumnInfo.GetPropertyType();
            }
        }

        public string GetGeneratedPropertyLine()
        {
            return $"public {PropertyType} {PropertyName} {{ get; set; }}";
        }

        public string GetGeneratedPropertyLine(bool forceNullable, bool forceVirtual)
        {
            var nullablePropertyType = ColumnInfo.GetPropertyType(forceNullable);
            var virtualStr = forceVirtual ? "virtual" : "";
            return $"public {virtualStr} {nullablePropertyType} {PropertyName} {{ get; set; }}";
        }
    }
}

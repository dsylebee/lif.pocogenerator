﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIF.PocoGenerator.Models.Schema
{
    public class TableInformation
    {
        public string Schema { get; set; }
        public string Name { get; set; }

        public List<ColumnInformation> Columns { get; set; } = new List<ColumnInformation>();
        public List<ColumnInformation> PrimaryKeys { get; set; } = new List<ColumnInformation>();

        public List<ForeignKeyInformation> ForeignKeys { get; set; } = new List<ForeignKeyInformation>();
        public List<ForeignKeyInformation> ReverseNavigation { get; set; } = new List<ForeignKeyInformation>();

        public override string ToString()
        {
            return $"[{Schema}].[{Name}]";
        }

        public bool IsPrimaryKey(ColumnInformation columnInformation)
        {
            return PrimaryKeys.Contains(columnInformation);
        }

        public int? PrimaryKeyOrder(ColumnInformation columnInformation)
        {
            for(var i = 0; i < PrimaryKeys.Count; i++)
            {
                if (PrimaryKeys[i] == columnInformation)
                    return i + 1;
            }

            return null;
        }

        public bool IsManyToManyTable()
        {
            // two foreign key primary keys is many to many.
            if (ForeignKeys.Count == 2 && this.Columns.Count == 2)
            {
                return ForeignKeys[0].ForeignKeyColumn.IsPrimaryKey && ForeignKeys[1].ForeignKeyColumn.IsPrimaryKey;
            }

            return false;
        }

        public List<ForeignKeyInformation> HasOne()
        {
            var ret = ReverseNavigation.Where(t => t.IsHasOne()).ToList();
            return ret;
        }

        public List<ForeignKeyInformation> HasMany()
        {
            var ret = ReverseNavigation.Where(t => t.IsHasMany()).ToList();
            return ret;
        }

        public List<ForeignKeyInformation> ManyToMany()
        {
            var ret = ReverseNavigation.Where(t => t.IsManyToMany()).ToList();
            return ret;
        }       
    }
}

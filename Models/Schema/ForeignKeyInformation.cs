﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIF.PocoGenerator.Models.Schema
{
    public class ForeignKeyInformation
    {
        public ColumnInformation ForeignKeyColumn { get; set; }
        public ColumnInformation PrimaryKeyColumn { get; set; }

        public bool IsManyToMany()
        {
            return ForeignKeyColumn.Table.IsManyToManyTable();
        }

        public bool IsHasOne()
        {
            if (ForeignKeyColumn.Table.PrimaryKeys.Count() == 1 && ForeignKeyColumn.IsPrimaryKey)
                return true;

            return false;
        }

        public bool IsHasMany()
        {
            return !IsHasOne() && !IsManyToMany();
        }

        public override string ToString()
        {
            var ret = "";

            if (ForeignKeyColumn != null)
                ret += ForeignKeyColumn.ToString();

            if (PrimaryKeyColumn != null)
                ret += $" -> " + PrimaryKeyColumn.ToString();

            return ret;
        }
    }
}

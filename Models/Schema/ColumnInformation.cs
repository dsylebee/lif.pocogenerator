﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIF.PocoGenerator.Models.Schema
{
    public partial class ColumnInformation
    {
        public static readonly List<string> stringTypes = new List<string>
        {
            "nvarchar", "nchar", "ntext",
            "varchar", "char", "text"
        };

        public static readonly List<string> fixedLengthTypes = new List<string>()
        {
            "char", "nchar", "binary"
        };

        public static readonly Dictionary<string, Type> mapping = new Dictionary<string, Type>
        {
            ["bigint"] = typeof(long),
            ["smallint"] = typeof(short),
            ["int"] = typeof(int),
            ["uniqueidentifier"] = typeof(Guid),
            ["smalldatetime"] = typeof(DateTime),
            ["datetime"] = typeof(DateTime),
            ["datetime2"] = typeof(DateTime),
            ["date"] = typeof(DateTime),
            ["datetimeoffset"] = typeof(DateTimeOffset),
            ["table type"] = typeof(System.Data.DataTable),
            ["time"] = typeof(TimeSpan),
            ["float"] = typeof(double),
            ["real"] = typeof(float),
            ["numeric"] = typeof(decimal),
            ["smallmoney"] = typeof(decimal),
            ["decimal"] = typeof(decimal),
            ["money"] = typeof(decimal),
            ["tinyint"] = typeof(byte),
            ["bit"] = typeof(bool),

            ["image"] = typeof(byte[]),
            ["binary"] = typeof(byte[]),
            ["varbinary"] = typeof(byte[]),
            ["varbinary(max)"] = typeof(byte[]), // ?? is this really a sql server type?
            ["timestamp"] = typeof(byte[]),

            ["geography"] = typeof(System.Data.Entity.Spatial.DbGeography),
            ["geometry"] = typeof(System.Data.Entity.Spatial.DbGeometry)

            // i think this one is depercated.
            //["hierarchyid"] =  System.Data.Entity.Hierarchy.HierarchyId,
        };
    }

    public partial class ColumnInformation
    {
        // colun info
        public string DataType { get; set; }
        public string Name { get; set; }
        public bool Nullable { get; set; }
        public int? CharacterMaximumLength { get; set; }
        public string DefaultValue { get; set; } = null;
        public byte? NumericPrecision { get; set; } = null;
        public byte? NumericScale { get; set; } = null;

        public bool IsValueType
        {
            get
            {
                if (mapping.ContainsKey(DataType))
                    return mapping[DataType].IsValueType;

                return false;
            }
        }

        public bool IsFixedLength
        {
            get
            {
                return fixedLengthTypes.Contains(DataType);
            }
        }

        public bool IsStringType
        {
            get
            {
                return stringTypes.Contains(DataType);
            }
        }

        public bool IsUnicode
        {
            get
            {
                return DataType == "nvarchar" || DataType == "nchar" || DataType == "ntext";
            }
        }

        public bool IsPrimitive
        {
            get
            {
                if (mapping.ContainsKey(DataType))
                    return mapping[DataType].IsPrimitive;

                return false;
            }
        }
        
        public bool HasDefault
        {
            get
            {
                return !string.IsNullOrEmpty(DefaultValue);
            }
        }

        public bool IsPrimaryKey
        {
            get
            {
                return Table == null ? false : Table.IsPrimaryKey(this);
            }
        }

        public int? PrimaryKeyOrder
        {
            get
            {
                if (Table != null)
                    return Table.PrimaryKeyOrder(this);                  

                return null;
            }
        }

        // table.
        public TableInformation Table { get; set; }
        public bool IsIdentity { get; set; }

        public string GetPropertyType(bool forceNullable = false)
        {
            var ret = "string";

            if (mapping.ContainsKey(this.DataType))
            {
                var type = mapping[this.DataType];

                // return fullname.
                ret = type.FullName;

                if ((forceNullable || this.Nullable) && type.IsValueType)
                {
                    ret = $"System.Nullable<{ret}>";
                }
            }

            return ret;
        }

        // just for debug display.
        public override string ToString()
        {
            var ret = $"[{Name}] {DataType} {(Nullable ? "NULL" : "NOT NULL")} {(HasDefault ? $"DEFAULT {DefaultValue}" : "")}";
            if (Table != null)
                ret += $" ({Table.ToString()})";

            return ret;
        }
    }
}
